from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name="home"), #
    path('about_us', views.about_us, name="about_us"), #
    path('contact', views.contact, name="contact"), #
    path('shop', views.shop, name="shop"), #
    path('single_product/<int:id>/', views.single_product, name="single_product"), #
    path('blog', views.blog, name="blog"), #
    path('blog_detail', views.blog_detail, name="blog_detail"), #
    path('checkout', views.checkout, name="checkout"), #
    path('account', views.account, name="account"), #
    path('login', views.login, name="login"), #
    path('logout', views.logout, name="logout"), #
    path('register', views.register, name="register"),
    path('wishlist', views.wishlist, name="wishlist"),
    path('searchbar',views.searchbar, name="searchbar"),
    path('thankyou',views.thankyou, name="thankyou"),
    path('wishlist_delete/<int:id>/',views.wishlist_delete, name='wishlist_delete'),

    # cart ko
   path('cart/add/<int:id>/', views.cart_add, name='cart_add'),
    path('cart/item_clear/<int:id>/', views.item_clear, name='item_clear'),
    path('cart/item_increment/<int:id>/',
         views.item_increment, name='item_increment'),
    path('cart/item_decrement/<int:id>/',
         views.item_decrement, name='item_decrement'),
    path('cart/cart_clear/', views.cart_clear, name='cart_clear'),
    path('cart/cart-detail/',views.cart_detail,name='cart_detail'),
    path('cart/checkout/placeorder', views.placeOrder, name="placeorder") 
]