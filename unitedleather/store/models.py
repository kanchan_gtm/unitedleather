from datetime import datetime
from statistics import mode
from django.db import models
from django.contrib.auth.models import User
from django.utils.html import mark_safe # sending data safely to the djangoadmin
import datetime
# Create your models here.

# RichTextField!!!!!!!!!!!!!!!!!!!!!!!!!!!!

# class Customer(models.Model):
#     user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
#     name = models.CharField(max_length=200, null=True)
#     email = models.CharField(max_length=200, null=True)
#     phone = models.CharField(max_length=200, null=True)

#     def __str__(self):
#         return self.name   
class Color(models.Model):
    title = models.CharField(max_length=200, null=True)
    color_code = models.CharField(max_length=200, null=True)

    def color_bg(self):
        return mark_safe('<div style="width:30px; height:30px; background-color:%s"></div>' % (self.color_code))

    def __str__(self):
        return self.title

    class Meta:
       verbose_name_plural = "Color"  

class Size(models.Model):
    title=models.CharField(max_length=100)

    def __str__(self):
        return self.title

    class Meta:
       verbose_name_plural = "Size"  

class Category(models.Model):
    title = models.CharField(max_length=200, null=True)
    sub_title = models.CharField(max_length=200, null=True)
    item_name = models.CharField(max_length=200, null=True)
    pub_date = models.DateField(default=datetime.datetime.today)


    def __str__(self):
        return self.title

    class Meta:
       verbose_name_plural = "Category"  

class Product(models.Model):
    name = models.CharField(max_length=200, null=True)
    image = models.ImageField(null=True, blank=True)
    description = models.TextField(max_length=500, null=True, blank=True)
    pub_date = models.DateField(default=datetime.datetime.today)
    price = models.PositiveIntegerField(null=True)  
    is_featured = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    is_new = models.BooleanField(default=False)
    color=models.ForeignKey(Color,on_delete=models.CASCADE, blank=True, null=True)
    category = models.ForeignKey(Category,on_delete=models.CASCADE, blank=True, null=True)

    def image_tag(self):
        return mark_safe('<img src="%s" width="50" height="50" />' % (self.image.url))


    def __str__(self):
        return self.name

        
    class Meta:
       verbose_name_plural = "Product" 

    @property
    def imageURL(self):
        try:
            url = self.image.url
        except:
            url = ""
        return url

class Banner(models.Model):
    image = models.ImageField(null=True, blank=True)

    class Meta:
       verbose_name_plural = "Banner"  

class Contact(models.Model):
    name = models.CharField(max_length=122, null=True, blank=True)
    email = models.CharField(max_length=122, null=True, blank=True)
    telephone = models.CharField(max_length=200, null=True, blank=True)
    comment = models.CharField(max_length=200, null=True, blank=True)
    date = models.DateField(default=datetime.datetime.today)  

    def __str__(self):
        return self.name

    class Meta:
       verbose_name_plural = "Contact"  

class Order(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE, blank=True, null=True)
    firstname = models.CharField(max_length=122, null=True, blank=True)
    lastname = models.CharField(max_length=122, null=True, blank=True)
    email = models.CharField(max_length=122, null=True, blank=True)
    phone = models.CharField(max_length=122, null=True, blank=True)
    address = models.CharField(max_length=122, null=True, blank=True)
    city = models.CharField(max_length=122, null=True, blank=True)
    country = models.CharField(max_length=122, null=True, blank=True)
    state_province = models.CharField(max_length=122, null=True, blank=True)
    amount = models.CharField(max_length=122, null=True, blank=True)
    payement_id = models.CharField(max_length=122, null=True, blank=True)
    paid = models.BooleanField(default=False, null=True)
    date = models.DateField(default=datetime.datetime.today)

    def __str__(self):
        return self.user.username

    class Meta:
       verbose_name_plural = "Order"  

class OrderItem(models.Model):
    order = models.ForeignKey(Order,on_delete=models.CASCADE, blank=True, null=True)
    product = models.CharField(max_length=122, null=True)
    image = models.ImageField(null=True, blank=True)
    quantity = models.CharField(max_length=122, null=True)
    price = models.CharField(max_length=122, null=True)
    total = models.CharField(max_length=122, null=True)
    date = models.DateField(default=datetime.datetime.today)

    def __str__(self):
        return self.order.user.username

    class Meta:
       verbose_name_plural = "OrderItem"  

    @property
    def imageURL(self):
        try:
            url = self.image.url
        except:
            url = ""
        return url

class Wishlist(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE, blank=True, null=True)
    product = models.ForeignKey(Product,on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.user.username

    class Meta:
       verbose_name_plural = "Whislist"  



class AboutUs(models.Model):
    image = models.ImageField(null=True, blank=True)
    heading = models.TextField(max_length=122, null=True)
    description = models.TextField(max_length=500, null=True)

    def __str__(self):
        return self.heading

    @property
    def imageURL(self):
        try:
            url = self.image.url
        except:
            url = ""
        return url

    class Meta:
       verbose_name_plural = "AboutUs" 

class Team(models.Model):
    image = models.ImageField(null=True, blank=True)
    name = models.CharField(max_length=122, null=True)
    post = models.CharField(max_length=122, null=True)
    fb_link = models.CharField(max_length=122, null=True)
    twitter_link = models.CharField(max_length=122, null=True)

    def __str__(self):
        return self.name

    @property
    def imageURL(self):
        try:
            url = self.image.url
        except:
            url = ""
        return url

    class Meta:
       verbose_name_plural = "Team" 

class Company(models.Model):
    name = models.CharField(max_length=122, null=True)
    phone =  models.CharField(max_length=122, null=True)
    email = models.CharField(max_length=122, null=True)
    short_description =  models.TextField(max_length=200, null=True)
    fb_link = models.CharField(max_length=122, null=True)  
    twiiter_link = models.CharField(max_length=122, null=True)
    google =  models.CharField(max_length=122, null=True)

    class Meta:
       verbose_name_plural = "Company" 




