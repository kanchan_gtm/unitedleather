# Generated by Django 3.2 on 2022-01-27 06:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0022_auto_20220125_1401'),
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=122, null=True)),
                ('phone', models.CharField(max_length=122, null=True)),
                ('email', models.CharField(max_length=122, null=True)),
                ('short_description', models.CharField(max_length=200, null=True)),
                ('fb_link', models.CharField(max_length=122, null=True)),
                ('twiiter_link', models.CharField(max_length=122, null=True)),
                ('google', models.CharField(max_length=122, null=True)),
            ],
        ),
    ]
