# Generated by Django 3.2 on 2022-01-17 09:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0005_rename_image_product_img'),
    ]

    operations = [
        migrations.RenameField(
            model_name='product',
            old_name='img',
            new_name='image',
        ),
    ]
