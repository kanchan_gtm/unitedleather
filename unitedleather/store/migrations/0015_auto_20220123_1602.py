# Generated by Django 3.2 on 2022-01-23 10:17

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('store', '0014_rename_title_product_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('firstname', models.CharField(blank=True, max_length=122, null=True)),
                ('lastname', models.CharField(blank=True, max_length=122, null=True)),
                ('email', models.CharField(blank=True, max_length=122, null=True)),
                ('phone', models.CharField(blank=True, max_length=122, null=True)),
                ('address1', models.CharField(blank=True, max_length=122, null=True)),
                ('address2', models.CharField(blank=True, max_length=122, null=True)),
                ('city', models.CharField(blank=True, max_length=122, null=True)),
                ('country', models.CharField(blank=True, max_length=122, null=True)),
                ('state_province', models.CharField(blank=True, max_length=122, null=True)),
                ('amount', models.CharField(blank=True, max_length=122, null=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AlterField(
            model_name='product',
            name='category',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='store.category'),
        ),
        migrations.CreateModel(
            name='OrderItem',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('product', models.CharField(max_length=122, null=True)),
                ('image', models.ImageField(blank=True, null=True, upload_to='')),
                ('quantity', models.CharField(max_length=122, null=True)),
                ('price', models.CharField(max_length=122, null=True)),
                ('total', models.CharField(max_length=122, null=True)),
                ('order', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='store.order')),
            ],
        ),
    ]
