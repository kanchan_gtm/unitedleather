import email
from email.mime import image
from itertools import product
from django.http.response import HttpResponse
from multiprocessing import context
from django.shortcuts import redirect, render
from django.test import client
from store.models import Category, OrderItem,Product,Color,Contact,Order,AboutUs,Team,Wishlist,Company
from django.db.models import Q
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login as dj_login, logout as dj_logout
from django.contrib.auth.decorators import login_required
from cart.cart import Cart
from django.db.models import Q
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator

# Create your views here.
def home(request):
   product_list = Product.objects.all().order_by('-id')[:6]
   featured_list = Product.objects.filter(is_featured = True)[:4]
   new_products = Product.objects.filter(is_new = True)[:4]
   category_list = Category.objects.all().order_by('-id')
   category = request.GET.get('category')
   print(category)
   company = Company.objects.first()     
   context = {'category_list': category_list, 'featured_list' : featured_list, 'new_products' : new_products, 'product_list': product_list, 'company':company}
   return render(request, 'store/index.html', context)

def shop(request):
   category = request.GET.get('category')
   color = request.GET.get('color')
   NAMEID = request.GET.get('name')
   print(NAMEID)
   PRICE = request.GET.get('price')
   NEW = request.GET.get('is_new')
   # OLD = request.GET.get('old')
   if category == None:
      category_list = Category.objects.all().order_by('-id')
      colors = Color.objects.all()     
   else:
      category_list = Category.objects.all().order_by('-id')
      colors = Color.objects.all()




   query = Q()
   if color:
      query &= Q(color__title=color)

   if category:
      query &= Q(category__title=category)


   # if color == None:
   #    product_list = Product.objects.all() 
   #    category_list = Category.objects.all().order_by('-id')
   #    colors = Color.objects.all()
   # else:
   #    product_list = Product.objects.filter(color__title = color)
   #    category_list = Category.objects.all().order_by('-id')
   #    colors = Color.objects.all()
   sort = 'name'
   if NAMEID:
      sort = 'name'


   elif PRICE:
      sort = 'price'
      

   elif NEW:
      sort = 'new'
   product_list = Product.objects.filter(query).order_by(sort)
   paginator = Paginator(product_list, 3)
   page_number = request.GET.get('page') #url ma dekni value get garni ani page_number ma rakhni
   try:
      page_obj = paginator.get_page(page_number)
   except PageNotAnInteger:
      page_obj = paginator.page(1)
   except EmptyPage:
      page_obj = paginator.page(paginator.num_pages)

   # elif OLD:
   #    product_list = Product.objects.filter().order_by('old')
   company = Company.objects.first()
   context = {'product_list':product_list, 'category_list':category_list, 'colors': colors, 'company':company,'page_obj':page_obj}
   return render(request, 'store/shop.html', context)

def single_product(request,id):
   
   product = Product.objects.get(id=id)
   category_list = Category.objects.all().order_by('-id')
   colors= Color.objects.all()
   company = Company.objects.first()
   context = {'product':product,'category_list':category_list,'colors':colors, 'company':company}
   return render(request, 'store/single-product.html', context) 

def searchbar(request):
   category_list = Category.objects.all().order_by('-id')
   company = Company.objects.first()
   colors= Color.objects.all()
   if 'query' in request.GET:
        query = request.GET['query']
        products = Product.objects.filter(Q(name__icontains=query))
        return render(request, 'store/searchbar.html', {'products':products,'category_list':category_list,'company':company,'colors':colors})

   else:
        print("No information to show")
        return render(request, 'store/searchbar.html', {'category_list':category_list, 'company':company,'colors':colors}) 

def about_us(request):
   about_list = AboutUs.objects.first()
   team_list = Team.objects.all()
   company = Company.objects.first()
   context = {'about_list':about_list, 'team_list':team_list,'company':company}
   return render(request, 'store/about-us.html', context)


def contact(request):
   company = Company.objects.first()
   category_list = Category.objects.all().order_by('-id')
   if request.method == 'POST':
      name = request.POST['name']
      email = request.POST['email']
      telephone = request.POST['telephone']
      comment = request.POST['comment']

      contact = Contact(
         name = name,
         email = email,
         telephone = telephone,
         comment = comment
      )
      contact.save()
      messages.success(request, "Message Sent!!")
   context = {'category_list' :category_list,'company':company}
   return render(request, 'store/contact.html', context)

def blog(request):
   return render(request, 'store/blog.html')

def checkout(request):
   company = Company.objects.first()
   category_list = Category.objects.all().order_by('-id')
   colors= Color.objects.all()
   # payment = client.order.create({
   #    "amount" :500,
   # })
   # order_id = payment['id']
   # context = {
   #    'order_id' : order_id,
   #    'payment' : payment
   # }
   # print(payment,order_id)
   return render(request, 'store/checkout.html', {'company': company, 'category_list':category_list,'colors':colors})

def blog_detail(request):
   return render(request, 'store/blog-detail.html')

def account(request):
   return render(request, 'store/account.html')

def login(request):
   category_list = Category.objects.all().order_by('-id')
   company = Company.objects.first()
   context = {'category_list' : category_list,'company':company}
   if request.method == 'POST':
      username = request.POST.get('username')
      password = request.POST.get('password') 

      user = authenticate(username = username, password = password)
      if user is not None:
         dj_login(request,user)
         return redirect('home')
      else:
         return redirect('login')

   return render(request, 'store/login.html', context)

def logout(request):
   dj_logout(request)
   return redirect('home')

def register(request):
   category_list = Category.objects.all().order_by('-id')
   context = {'category_list' : category_list}
   if request.method == 'POST':
      username = request.POST.get('username')
      first_name = request.POST.get('first_name')
      last_name = request.POST.get('last_name')
      email = request.POST.get('email')
      pass1 = request.POST.get('pass1')
      pass2 = request.POST.get('pass2')

      customer = User.objects.create_user(username,email,pass1)
      customer.first_name = first_name
      customer.last_name = last_name
      customer.save()
      return redirect('login')
   return render(request, 'store/register.html', context)

@login_required(login_url="/login")
def wishlist(request):  
   company = Company.objects.first()
   category_list = Category.objects.all().order_by('-id')
   uid = request.session.get('_auth_user_id')
   user = User.objects.get(id = uid)
   if(not user):
      #  rediret to login page
      return redirect('/login')
   else:
      wishlist = Wishlist.objects.all() 
      if request.method == 'POST':
         product_id = request.POST['product_id']
         print("product_id--------",product_id)
         product = Product.objects.get(id = product_id)      
         if(product in wishlist):
            messages.info(request, "Already in wishlist")
         else:
            wishlist = Wishlist(user = user, product = product)
            print(product)
            wishlist.save()
            messages.success(request, "Added in wishlist")
      # wishlist = Wishlist.objects.filter(user= user)
   context = {'wishlist':wishlist,'company':company,'category_list':category_list}
   return render(request, 'store/wishlist.html', context)

@login_required(login_url="/login")
def wishlist_delete(request, id):
    uid = request.session.get('_auth_user_id')
    user = User.objects.get(id = uid)   
    print('user',user)
    product = Product.objects.get(id=id)
    wishlist = Wishlist.objects.get(user = user, product = product)
    wishlist.delete()
    return redirect('/wishlist')


@login_required(login_url="/login")
def cart_add(request, id):
    cart = Cart(request)
    product = Product.objects.get(id=id)
    cart.add(product=product)
    return redirect("home")


@login_required(login_url="/login")
def item_clear(request, id):
    cart = Cart(request)
    product = Product.objects.get(id=id)
    cart.remove(product)
    return redirect("cart_detail")


@login_required(login_url="/login")
def item_increment(request, id):
    cart = Cart(request)
    product = Product.objects.get(id=id)
    cart.add(product=product)
    return redirect("cart_detail")


@login_required(login_url="/login")
def item_decrement(request, id):
    cart = Cart(request)
    product = Product.objects.get(id=id)
    cart.decrement(product=product)
    return redirect("cart_detail")


@login_required(login_url="/login")
def cart_clear(request):
    cart = Cart(request)
    cart.clear()
    return redirect("cart_detail")


@login_required(login_url="/login")
def cart_detail(request):
   company = Company.objects.first()
   category_list = Category.objects.all().order_by('-id')
   context = {'company':company,'category_list':category_list}
   return render(request, 'store/cart.html', context)

def placeOrder(request):
   category_list = Category.objects.all().order_by('-id')
   company = Company.objects.first()
   if request.method == 'POST':
      uid = request.session.get('_auth_user_id')
      user = User.objects.get(id = uid)
      print(user)
      cart = request.session.get('cart')
      print(cart)
      firstname = request.POST.get('firstname')
      lastname = request.POST.get('lastname')
      email = request.POST.get('email')
      phone = request.POST.get('phone')
      address = request.POST.get('address')
      city = request.POST.get('city')
      country = request.POST.get('country')
      state_province = request.POST.get('state_province')
      amount = request.POST.get('amount')
      order_id = request.POST.get('order_id')
      # payment = request.POST.get('payment')
      order = Order(
      user = user,
      firstname = firstname,
      lastname = lastname,
      email = email,
      phone = phone,
      address = address,
      city = city,
      country = country,
      state_province = state_province,
      # payment_id = order_id,
      amount = amount,
      ) 
      order.save()
      for i in cart:
         a = int(cart[i]['price'])
         b = int(cart[i]['quantity'])

         total = a * b
         print(total)
         # print(a)
         # print(b)
         # print(type(a))
         # print(type(b))

         item = OrderItem(
            order = order,
            product = cart[i]['name'],
            image = cart[i]['image'],
            quantity = cart[i]['quantity'],
            price = cart[i]['price'],
            total = total
         )
         item.save()
      # print(amount)
      # print(order_id)
   context = {'category_list':category_list,'company':company} 
   return render(request, 'store/placeorder.html', context)

def thankyou(request):
   category_list = Category.objects.all().order_by('-id')
   company = Company.objects.first()
   context = {'category_list':category_list,'company':company}
   return render(request, 'store/thankyou.html', context)  
