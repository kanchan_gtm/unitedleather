from django.contrib import admin
from .models import *

# Register your models here.
admin.site.site_header = "Unitedleather Adminstration"
admin.site.site_title = "United Leather"
admin.site.index_title = "Welcome to this portal"


class ColorAdmin(admin.ModelAdmin):
    list_display = ('title', 'color_bg')

class ProductAdmin(admin.ModelAdmin):
    list_display = ('id','name','price','is_featured','is_active','image_tag','color')
    list_editable = ('is_active','is_featured')

class OrderItemTubleinline(admin.TabularInline): #yo garda euta table ma dubai model dekhinxa
    model = OrderItem

class OrderAdmin(admin.ModelAdmin):
    inlines = [OrderItemTubleinline]
    list_display = ['firstname', 'phone', 'email','date']
    search_fields = ['firstname','email']

# add image tag sabaima....
admin.site.register(Category)
admin.site.register(Contact)
admin.site.register(Product,ProductAdmin)
admin.site.register(Color,ColorAdmin)
admin.site.register(Order,OrderAdmin)
admin.site.register(OrderItem)
admin.site.register(Banner)
admin.site.register(Size)
admin.site.register(AboutUs)
admin.site.register(Wishlist)
admin.site.register(Team)
admin.site.register(Company)
